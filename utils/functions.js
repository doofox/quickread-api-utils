'use strict'

/**
 * 确保内容为正整数
 * @param { Number } number - 要被处理的数字
 * @return { Number } - 返回正整数
 */
function absint(number) {
	return Math.abs(parseInt(number))
}

/**
 * 序列化字符串 key,作为内部使用
 * Keys are used as internal identifiers. Lowercase alphanumeric characters, dashes and underscores are allowed.
 * @param { String } key - 要序列化的字符串
 * @return { String } 序列化完的 key
 */
function sanitizeKey(key) {
	return key.toLowerCase().replace(/[^a-z0-9_\-]/, '', key)
}

/**
 * 对象深拷贝
 * @param { Object } obj - 被拷贝的对象
 * @return { Object } 拷贝的对象
 */
function deepClone(obj) {
	return JSON.parse(JSON.stringify(obj))
}

module.exports = { absint, sanitizeKey, deepClone }
