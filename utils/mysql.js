'use strict'

const mysql = require('mysql')
const _ = require('lodash')

/**
 * 针对 mysql 的功能扩展
 */
class Mysql {
	/**
	 * 通过 query 查询时 获取单个值
	 * @param { Array } results - 查询结果
	 * @param { Number } x - 列 偏移值,默认 0 取第一个
	 * @param { Number } y - 行 偏移值,默认 0 取第一行
	 * @return { Any|Null } 返回单个值,失败返回 null
	 */
	getVar(results, x = 0, y = 0) {
		if (!results[y]) return null

		if (!Object.keys(results[y])[x]) return null

		const key = Object.keys(results[y])[x]
		return results[y][key]
	}

	/**
	 * 通过 query 查询时 获取单列值
	 * @param { Array } results - 查询结果
	 * @param { Number } x - 列 偏移值,默认 0 取第一个
	 * @return { Array|Null } 返回数组,失败返回 null
	 */
	getCol(results, x = 0) {
		if (!_.isArray(results) || results.length === 0) return null
		const key = Object.keys(results[0])[x]
		return results.map(data => data[key]) || []
	}

	/**
	 * 验证/拼接 sql 并返回符合格式的语句
	 * @param { String } query - sql 语句
	 * @param  { ...Any } val - 值
	 * @return { String } 返回安全的语句
	 */
	prepare(query, ...val) {
		return mysql.format(query, val)
	}
}

module.exports = Mysql
