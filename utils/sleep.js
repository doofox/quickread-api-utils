'use strict'

/**
 * 异步延迟
 * @param {number} time 延迟的时间,单位毫秒
 * @return {Promise} 延迟
 */
const sleep = (time = 0) => {
	return new Promise(resolve => {
		setTimeout(() => {
			resolve()
		}, time)
	})
}

module.exports = sleep
