'use strict'

const Serialize = require('./../../utils/serialize')
const sleep = require('./../../utils/sleep')
const functions = require('./../../utils/functions')
const Mysql = require('./../../utils/mysql')

module.exports = {
	Serialize: new Serialize(),
	sleep,
	func: functions,
	mysql: new Mysql()
}
