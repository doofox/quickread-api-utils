'use strict'

const Service = require('egg').Service
const _ = require('lodash')

/**
 * 缓存管理系统
 *
 * @class CacheService
 * @extends {Service}
 */
class CacheService extends Service {
	/**
	 * 获取指定内容
	 * @param { String|Number } key - 缓存的 key
	 * @param { String } group - 数据分组
	 * @return { Any<Promise>|Boolean<Promise> } 返回value 或 false
	 */
	async get(key, group = 'default') {
		const derivedKey = this._buildKey(key, group)
		const result = await this.app.redis.get(derivedKey)
		if (result) {
			return this.ctx.Serialize.maybeUnserialize(result)
		}
		return false
	}

	/**
	 * 添加一个缓存数据,如果它存在的话,否则不做操作
	 * @param { String|Number } key - 缓存的 key
	 * @param { Any } data - 缓存的数据,缓存的数据将被序列化
	 * @param { String } group - 数据分组
	 * @param { Number } expire - 缓存过期时间 以秒为单位
	 * @return { Boolean<Promise> } False on failure, true on success
	 */
	async add(key, data, group = '', expire = 0) {
		if (await this.exists(key, group)) {
			return false
		}
		return this.set(key, data, group, expire)
	}

	/**
	 * 设置缓存数据,如果数据存在将被覆盖
	 * @param { String|Number } key - 缓存的 key
	 * @param { Any } data - 缓存的数据,缓存的数据将被序列化
	 * @param { String } group - 数据分组
	 * @param { Number } expire - 缓存过期时间 以秒为单位
	 * @return { Boolean<Promise> } False on failure, true on success
	 */
	async set(key, data, group = '', expire = 0) {
		// 默认情况下 group 为 default
		if (group === '' || typeof group !== 'string') {
			group = 'default'
		}

		// 如果是对象,先深拷贝一份
		if (_.isObject(data)) {
			data = _.cloneDeep(data)
		}

		// 生成 key
		const derivedKey = this._buildKey(key, group)
		// 定义过期时间(保存的过期时间为时间戳)
		expire = this._validateExpiration(expire)

		let result
		if (expire) {
			result = await this.app.redis.set(
				derivedKey,
				this.ctx.Serialize.maybeSerialize(data),
				'ex',
				expire + parseInt(+new Date() / 1000)
			)
		} else {
			result = await this.app.redis.set(
				derivedKey,
				this.ctx.Serialize.maybeSerialize(data)
			)
		}

		return result === 'OK'
	}

	/**
	 * 删除指定的 key
	 * @param { String|Number } key - 缓存的 key
	 * @param { String } group - 数据分组
	 * @return { Boolean<Promise> } 返回 是否删除成功
	 */
	async delete(key, group = 'default') {
		const derivedKey = this._buildKey(key, group)
		return (await this.app.redis.del(derivedKey)) !== 0
	}

	/**
	 * 批量删除 keys,可将事件加入队列统一执行,效率更高
	 * @param { Array<String|Number> } keys - 缓存的 keys
	 * @param { String } group - 数据分组
	 * @return { Array<Promise> } 被删除的状态 [[null, 'OK']]
	 */
	async deletes(keys, group = 'default') {
		const pipeline = this.app.redis.pipeline()
		for (const i in keys) {
			const derivedKey = this._buildKey(keys[i], group)
			pipeline.del(derivedKey)
		}
		return pipeline.exec()
	}

	/**
	 * 清空所有的缓存，并返回删除数量
	 * @return { Number<Promise> } 返回删除的数量
	 */
	flush() {
		const salt = this.app.config.apiUtils.cacheKeySalt + ':'
		return new Promise(resolve => {
			this.app.redis.defineCommand('echo', {
				numberOfKeys: 1,
				lua: `
					local i = 0
					for _,k in ipairs(redis.call('keys', '${salt}*')) do
						redis.call('del', k)
						i = i + 1
					end
					return i
				`
			})

			this.app.redis.echo('', function(err, result) {
				resolve(result)
			})
		})
	}

	/**
	 * 检查缓存是否存在
	 * @param { String|Number } key - 缓存的 key
	 * @param { String? } group - 数据分组 默认无分组
	 * @return { Boolean } 返回是否存在
	 */
	async exists(key, group = '') {
		return (await this.get(key, group)) !== false
	}

	/**
	 * 封装 key
	 * 将 group 放在 名称上,使分组平铺
	 * @param { String|Number } key - 缓存的 key
	 * @param { String } group - 数据分组
	 * @return { String } 返回新的 key
	 */
	_buildKey(key, group = 'default') {
		if (!group) {
			group = 'default'
		}

		// 加 salt 的用处是,通过 script 清空数据时,可只清空 salt 开头的数据（多站点使用同一个 DB 可互不影响）
		return `${this.app.config.apiUtils.cacheKeySalt}:${group}:${key}`
	}

	/**
	 * 验证并返回正确的过期时间
	 * 如果过期时间为 0 或者大于最大过期时间则直接返回最大过期时间，因为不允许设置永久的缓存时间
	 * @param { Number } expire - 过期时间(单位秒)
	 * @return { Number } 返回过期时间
	 */
	_validateExpiration(expire) {
		// eslint-disable-next-line eqeqeq
		expire = _.isNumber(expire) || +expire == expire ? parseInt(expire) : 0

		const max = this.app.config.apiUtils.cacheMaxTTL
		if (expire <= 0 || expire > max) {
			return max
		}
		return expire
	}
}
module.exports = CacheService
