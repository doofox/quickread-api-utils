'use strict'

const Metadata = require('../../class/metadata')

/**
 * metadata
 *
 * @class MetadataService
 * @extends {Service}
 */
class MetadataService extends Metadata {
	/**
	 * 获取用户 metadata
	 * @param { Number } userId - 用户 ID
	 * @param { String } metaKey - key
	 * @param { Boolean } single - 是否唯一, 默认false 返回一个数组 否则取第一个
	 * @return { Any<Promise> } 返回 metadata,没有返回空字符串或空数组
	 */
	async getUserMeta(userId, metaKey, single = false) {
		return this.getMetaData('user', userId, metaKey, single)
	}
	/**
	 * 添加用户 Meta
	 * @param { Number } userId - 用户 ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value
	 * @param { Boolean } unique - 是否为唯一的 key,默认 false,当key 已存在时,如果是 true 则不会继续添加也不会修改
	 * @return { Promise<Number|Boolean> } 成功返回添加meta ID
	 */
	async addUserMeta(userId, metaKey, metaValue, unique = false) {
		return this.addMetaData('user', userId, metaKey, metaValue, unique)
	}
	/**
	 * 更新用户 Meta
	 * @param { Number } userId - 用户 ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value
	 * @param { Any } prevValue - value 要匹配的值,默认 空.如果填写此值则只会更新匹配的 value,否则更新所有的条目
	 * @return { Boolean<Promise> } 成功返回 true
	 */
	async updateUserMeta(userId, metaKey, metaValue, prevValue = '') {
		return this.updateMetaData('user', userId, metaKey, metaValue, prevValue)
	}
	/**
	 * 删除指定用户 meta
	 * @param { Number } userId - 要删除的用户 ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value 默认空 如果不为空删除所有匹配的metaKey 否则删除匹配 Value 的 meta
	 * @return { Promise<Boolean> } 返回是否删除成功
	 */
	async deleteUserMeta(userId, metaKey, metaValue = '') {
		return this.deleteMetaData('user', userId, metaKey, metaValue)
	}

	/**
	 * 获取文章 meta
	 * @param { String } postType - 文章类型
	 * @param { Number } postId - 文章 ID
	 * @param { String } metaKey - key
	 * @param { Boolean } single - 是否唯一, 默认false 返回一个数组 否则取第一个
	 * @return { Any<Promise> } 返回 metadata,没有返回空字符串或空数组
	 */
	async getPostMeta(postType, postId, metaKey, single = false) {
		postType = `p_${postType}`
		return this.getMetaData(postType, postId, metaKey, single)
	}
	/**
	 * 添加文章 Meta
	 * @param { String } postType - 文章类型
	 * @param { Number } postId - 文章 ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value
	 * @param { Boolean } unique - 是否为唯一的 key,默认 false,当key 已存在时,如果是 true 则不会继续添加也不会修改
	 * @return { Promise<Number|Boolean> } 成功返回添加meta ID
	 */
	async addPostMeta(postType, postId, metaKey, metaValue, unique = false) {
		postType = `p_${postType}`
		return this.addMetaData(postType, postId, metaKey, metaValue, unique)
	}
	/**
	 * 更新文章 Meta
	 * @param { String } postType - 文章类型
	 * @param { Number } postId - 文章 ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value
	 * @param { Any } prevValue - value 要匹配的值,默认 空.如果填写此值则只会更新匹配的 value,否则更新所有的条目
	 * @return { Boolean<Promise> } 成功返回 true
	 */
	updatePostMeta(postType, postId, metaKey, metaValue, prevValue = '') {
		postType = `p_${postType}`
		return this.updateMetaData(postType, postId, metaKey, metaValue, prevValue)
	}
	/**
	 * 删除指定文章 meta
	 * @param { String } postType - 文章类型
	 * @param { Number } postId - 文章 ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value 默认空 如果不为空删除所有匹配的metaKey 否则删除匹配 Value 的 meta
	 * @return { Promise<Boolean> } 返回是否删除成功
	 */
	deletePostMeta(postType, postId, metaKey, metaValue = '') {
		postType = `p_${postType}`
		return this.deleteMetaData(postType, postId, metaKey, metaValue)
	}
}
module.exports = MetadataService
