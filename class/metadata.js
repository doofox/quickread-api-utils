'use strict'

const Service = require('egg').Service

const _ = require('lodash')

class Metadata extends Service {
	/**
	 * 获取 metadata
	 * @param { String } metaType - 数据类型
	 * @param { Number } objectId - 对象ID
	 * @param { String? } metaKey - key, 如果 key 为空,则表示取所有的内容
	 * @param { Boolean } single - 是否唯一, 默认false 返回一个数组 否则取第一个
	 * @return { Any } 返回 默认值 ''||[] 或 value
	 */
	async getMetaData(metaType, objectId, metaKey = '', single = false) {
		if (!metaType || !_.isNumber(objectId)) return false

		objectId = this.ctx.func.absint(objectId)
		if (!objectId) return false

		let metaCache = await this.ctx.service.cache.get(
			objectId,
			`${metaType}_meta`
		)
		// 如果缓存数据不存在则更新缓存数据
		if (!metaCache) {
			metaCache = await this._updateMetaCache(metaType, [objectId])
			metaCache = metaCache[objectId]
		}

		// 如果不传 key, 返回所有的数据,需要注意的是这些数据没有被序列化
		if (!metaKey) {
			return metaCache
		}

		if (metaCache[metaKey]) {
			if (single) {
				return this.ctx.Serialize.maybeUnserialize(metaCache[metaKey][0])
			}
			return metaCache[metaKey].map(v => this.ctx.Serialize.maybeUnserialize(v))
		}

		// 返回默认值
		if (single) {
			return ''
		}
		return []
	}

	/**
	 * 添加 MetaData
	 * @param { String } metaType - 数据类型
	 * @param { Number } objectId - 对象ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value
	 * @param { Boolean } unique - 是否为唯一的 key,默认 false,当key 已存在时,如果是 true 则不会继续添加也不会修改
	 * @return { Promise<Number|Boolean> } - 成功返回ID,失败返回 false
	 */
	async addMetaData(metaType, objectId, metaKey, metaValue, unique = false) {
		if (!metaType || !metaKey || !_.isNumber(objectId)) return false

		objectId = this.ctx.func.absint(objectId)
		if (!objectId) return false

		const table = this._getMetaTable(metaType)
		if (!table) return false

		const column = this.ctx.func.sanitizeKey(
			this._getObjectSubtype(metaType) + '_id'
		)

		// 如果数据库中有值则不再继续添加
		if (
			unique &&
			this.ctx.mysql.getVar(
				await this.app.mysql.query(
					`SELECT COUNT(*) FROM ${table} WHERE meta_key = ? AND ${column} = ?`,
					[metaKey, objectId]
				)
			)
		) {
			return false
		}

		// 序列化值
		metaValue = this.ctx.Serialize.maybeSerialize(metaValue)

		const insert = {
			meta_key: metaKey,
			meta_value: metaValue
		}
		insert[column] = objectId

		const result = await this.app.mysql.insert(table, insert)
		if (result.affectedRows === 0) return false

		const mid = parseInt(result.insertId)

		// 在这个步骤移除相关的缓存
		this.ctx.service.cache.delete(objectId, `${metaType}_meta`)

		return mid
	}

	/**
	 * 更新指定 metadata
	 * @param { String } metaType - 数据类型
	 * @param { Number } objectId - 对象ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value 要保存的值
	 * @param { Any } prevValue - value 要匹配的值,默认 空.如果填写此值则只会更新匹配的 value,否则更新所有的条目
	 * @return { Promise<Number|Boolean> } 如果key 不存在则为添加返回 metaid ,否则返回 true 或 false
	 */
	async updateMetaData(metaType, objectId, metaKey, metaValue, prevValue = '') {
		if (!metaType || !metaKey || !_.isNumber(objectId)) {
			return false
		}

		objectId = this.ctx.func.absint(objectId)
		if (!objectId && !deleteAll) return false

		const table = this._getMetaTable(metaType)
		if (!table) return false

		const column = this.ctx.func.sanitizeKey(
			this._getObjectSubtype(metaType) + '_id'
		)

		// 如果没有提供 prevValue 则用现有的值做判断,如果有值且唯一则不做任何更新 返回 false
		if (!prevValue) {
			const oldValue = await this.getMetaData(metaType, objectId, metaKey)
			if (oldValue.length === 1) {
				// eslint-disable-next-line eqeqeq
				if (oldValue[0] == metaValue) {
					return false
				}
			}
		}

		const metaIds = this.ctx.mysql.getCol(
			await this.app.mysql.query(
				`SELECT meta_id FROM ${table} WHERE meta_key = ? AND ${column} = ?`,
				[metaKey, objectId]
			)
		)
		// 数据如果不存在则添加新的
		if (!metaIds || metaIds.length === 0) {
			return this.addMetaData(metaType, objectId, metaKey, metaValue)
		}

		const where = {}
		where[column] = objectId
		where.meta_key = metaKey

		if (prevValue) {
			where.meta_value = this.ctx.Serialize.maybeSerialize(prevValue)
		}

		// 序列化值
		metaValue = this.ctx.Serialize.maybeSerialize(metaValue)

		/**
		 * ALIRDS 存在 bug,无法使用 update 形式进行更新,请确认此问题被修复且 egg 已更新后再试
		 * https://github.com/ali-sdk/ali-rds/issues/66
		 */
		/**
		const result = await this.app.mysql.update(
			table,
			{ meta_value: metaValue },
			{ where }
		)
		*/
		const result = await this.app.mysql.query(
			`UPDATE ${table} SET meta_value = ? ${this.app.mysql._where(where)};`,
			[metaValue]
		)
		if (!result.affectedRows !== 0) {
			return false
		}

		// 删除对应缓存
		await this.ctx.service.cache.delete(objectId, `${metaType}_meta`)

		return true
	}

	/**
	 * 删除指定 metadata
	 * @param { String } metaType - 数据类型
	 * @param { Number } objectId - 对象ID
	 * @param { String } metaKey - key
	 * @param { Any } metaValue - value,如果提供此值,会删除匹配此内容的 meta,否则移除所有
	 * @param { Boolean? } deleteAll - 默认 false,如何为 true 将会忽略 objectId 删除所有符合条件的对象数据
	 */
	async deleteMetaData(
		metaType,
		objectId,
		metaKey,
		metaValue = '',
		deleteAll = false
	) {
		if (!metaType || !metaKey || (!_.isNumber(objectId) && !deleteAll)) {
			return false
		}

		objectId = this.ctx.func.absint(objectId)
		if (!objectId && !deleteAll) return false

		const table = this._getMetaTable(metaType)
		if (!table) return false

		const column = this.ctx.func.sanitizeKey(
			this._getObjectSubtype(metaType) + '_id'
		)

		metaValue = this.ctx.Serialize.maybeSerialize(metaValue)

		let query = this.ctx.mysql.prepare(
			`SELECT meta_id FROM ${table} WHERE meta_key = ?`,
			metaKey
		)
		if (!deleteAll) {
			query += this.ctx.mysql.prepare(` AND ${column} = ?`, objectId)
		}

		if (metaValue) {
			query += this.ctx.mysql.prepare(' AND meta_value = ?', metaValue)
		}

		const metaIds = this.ctx.mysql.getCol(await this.app.mysql.query(query))
		if (!metaIds || metaIds.length === 0) {
			return false
		}

		let objectIds = []
		if (deleteAll) {
			// 需要查询所有被改动的 object_id 以删除对应的内存缓存数据
			if (metaValue) {
				// 删除和 metaValue 有关数据
				objectIds = this.ctx.mysql.getCol(
					await this.app.mysql.query(
						`SELECT ${column} FROM ${table} WHERE meta_key = ? AND meta_value = ?`,
						[metaKey, metaValue]
					)
				)
			} else {
				// 所有
				objectIds = this.ctx.mysql.getCol(
					await this.app.mysql.query(
						`SELECT ${column} FROM ${table} WHERE meta_key = ?`,
						[metaKey]
					)
				)
			}

			// 数组去重
			objectIds = [...new Set(objectIds)]
		}

		// 删除数据
		const del = await this.app.mysql.query(
			`DELETE FROM ${table} WHERE meta_id IN(${metaIds.join(',')})`
		)
		if (del.affectedRows === 0) {
			return falase
		}
		// 删除缓存
		if (deleteAll) {
			await this.ctx.service.cache.deletes(objectIds, `${metaType}_meta`)
		} else {
			await this.ctx.service.cache.delete(objectId, `${metaType}_meta`)
		}

		return true
	}

	/**
	 * 更新指定对象在缓存中的数据并返回最新的数据
	 * metadata 的内存缓本质是存储在一个对象中,里面保存了当前objectid的所有metaKey 和 value
	 * @param { String } metaType - 数据类型
	 * @param { Array<Number> } objectIds - meta_ids! 对象 ids 可以同时更新多个缓存数据
	 * @return { Any } 更新完的数据
	 */
	async _updateMetaCache(metaType, objectIds) {
		if (!metaType || !_.isArray(objectIds) || objectIds.length === 0) {
			return false
		}
		const table = this._getMetaTable(metaType)
		if (!table) return false

		const column = this.ctx.func.sanitizeKey(
			this._getObjectSubtype(metaType) + '_id'
		)

		const idsString = objectIds.join(',')

		// 查询数据库中所有 meta
		const metaList = await this.app.mysql.query(
			`SELECT ${column}, meta_key, meta_value FROM ${table} WHERE ${column} IN (${idsString}) ORDER BY meta_id ASC`
		)

		const cache = {}

		for (const i in metaList) {
			const metaRow = metaList[i]

			const id = this.ctx.func.absint(metaRow[column])
			const metaKey = metaRow.meta_key
			const metaValue = metaRow.meta_value

			if (!cache[id]) cache[id] = {}
			if (!cache[id][metaKey] || !_.isArray(cache[id][metaKey])) {
				cache[id][metaKey] = []
			}

			// cache{ id: { ...key: [...value] } }
			cache[id][metaKey].push(metaValue)
		}

		for (const i in objectIds) {
			const id = objectIds[i]
			if (!cache[id]) cache[id] = {}

			// 设置缓存
			this.ctx.service.cache.set(id, cache[id], `${metaType}_meta`)
		}

		return cache
	}

	/**
	 * 获取对应 meta 类型的表名
	 * @param { String } metaType - meta 类型
	 */
	_getMetaTable(metaType) {
		// 这里可以检测一下表名是否存在,再做返回
		return this.config.tablePrefix + metaType + 'meta'
	}
	/**
	 * 获取对应 meta 类型的公共类型
	 * @param { String } metaType - meta 类型
	 */
	_getObjectSubtype(metaType) {
		switch (metaType) {
			case 'user':
				return 'user'
			default:
				return 'post'
		}
	}
}

module.exports = Metadata
