'use strict'

/**
 * egg-api-utils default config
 * @member Config#apiUtils
 * @property {String} SOME_KEY - some description
 */
exports.apiUtils = {
	// 缓存前缀
	cacheKeySalt: 'default',
	// 缓存最大过期时间
	cacheMaxTTL: 86400
}
