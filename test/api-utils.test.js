/* eslint-disable no-unreachable */
'use strict'

const mock = require('egg-mock')
describe('test/api-utils.test.js', () => {
	let app
	before(() => {
		app = mock.app({
			baseDir: 'apps/api-utils-test'
		})
		return app.ready()
	})

	after(() => app.close())
	afterEach(mock.restore)

	it('数据序列化测试', () => {
		return
		const ctx = app.mockContext()
		return (
			ctx.Serialize.maybeUnserialize(
				ctx.Serialize.maybeSerialize({ hello: 'world' })
			).hello === 'world'
		)
	})

	it('内存缓存测试', async () => {
		return
		const ctx = app.mockContext()
		// 添加
		await ctx.service.cache.set('testkey', { hello: 'world' }, 'tesetgroupkey')
		// 获取
		await ctx.service.cache.get('testkey', 'tesetgroupkey')
		// 删除
		await ctx.service.cache.delete('testkey', 'tesetgroupkey')
		// 清空缓存
		await ctx.service.cache.flush()
		return (await ctx.service.cache.flush()) === 0
	})

	it('metadata 测试', async () => {
		const ctx = app.mockContext()
		// 添加
		console.log(
			await ctx.service.metadata.updateMetaData(
				'user',
				1,
				'github_access_token',
				'1111111',
				'ffffffff'
			)
		)
	})
})
