/* eslint valid-jsdoc: "off" */

'use strict'

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
	/**
	 * built-in config
	 * @type {Egg.EggAppConfig}
	 **/
	const config = {}

	// use for cookie sign key, should change to your own and keep security
	config.keys = appInfo.name + '_1554719086427_6848'

	// add your middleware config here
	config.middleware = []

	// add your user config here
	const userConfig = {
		tablePrefix: 'qr_'
	}

	config.mysql = {
		// 单数据库信息配置
		client: {
			// host
			host: '120.27.211.150',
			// 端口号
			port: '3306',
			// 用户名
			user: 'quickread',
			// 密码
			password: 'SRbmIsakRWuaX17k',
			// 数据库名
			database: 'quickread',
			// 编码
			charset: 'utf8mb4'
		},
		// 是否加载到 app 上，默认开启
		app: true,
		// 是否加载到 agent 上，默认关闭
		agent: false
	}

	config.redis = {
		client: {
			port: 6379, // Redis port
			host: '120.27.211.150', // Redis host
			password: 'LTAIH4J10mrJgons',
			db: 0
		}
	}

	return {
		...config,
		...userConfig
	}
}
